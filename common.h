#ifndef __COMMON_H__
#define __COMMON_H__

#define DEPTH 10
#define MAX_SCORE 1000000000
#define MIN_SCORE -1000000000
#define GAME_WIN 1000000
#define GAME_LOSE -1000000

enum Side { 
    WHITE, BLACK
};

class Move {
   
public:
    int x, y;

    Move() {}

    Move(int x, int y) {
        this->x = x;
        this->y = y;        
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }

    void set(int x, int y) {
      this->x = x;
      this->y = y;
    }

    bool checkCorner() {
      if ((this->x == 0 && this->y == 0) ||
	  (this->x == 0 && this->y == 7) ||
	  (this->x == 7 && this->y == 0) ||
	  (this->x == 7 && this->y == 7)) {
	return true;
      }
      else {
	return false;
      }
    }

    bool checkDiagonalToCorner() {
      if ((this->x == 1 && this->y == 1) ||
	  (this->x == 1 && this->y == 6) ||
	  (this->x == 6 && this->y == 1) ||
          (this->x == 6 && this->y == 6)) {
		return true;
      }
      else {
	return false;
      }
    }

    bool checkAdjacentToCorner() {
      if ((this->x == 0 && this->y == 1) ||
	  (this->x == 1 && this->y == 0) ||
	  (this->x == 0 && this->y == 6) ||
	  (this->x == 1 && this->y == 7) ||
	  (this->x == 6 && this->y == 0) ||
	  (this->x == 7 && this->y == 1) ||
	  (this->x == 6 && this->y == 7) ||
	  (this->x == 7 && this->y == 6)) {
	return true;
      }
      else {
	return false;
      }
    }

    bool checkEdge() 
    {
       if ((this->x == 0 && (this->y >= 2 && this->y <= 5)) ||
           (this->x == 7 && (this->y >= 2 && this->y <= 5)) ||
           (this->y == 0 && (this->x >= 2 && this->x <= 5)) ||
           (this->y == 7 && (this->x >= 2 && this->x <= 5)))
       {
           return true;
       }
       else
       { 
           return false;
       }
    }   
       

};

#endif
