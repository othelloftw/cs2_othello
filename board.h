#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include <iostream>
#include "common.h"
using namespace std;

class Board {
   
 private:
  bitset<64> black;
  bitset<64> taken;

  Move bestMove;
  bool foundMove;
  
  std::pair<Move*, Side> lastPlay;

  Side color;
  Side opponentsColor;   
       
  bool occupied(int x, int y);
  bool get(Side side, int x, int y);
  void set(Side side, int x, int y);
  bool onBoard(int x, int y);
      
 public:
  Board();
  ~Board();
  Board *copy();
  
  
  void setBestMove(Move move);
  Move* getBestMove();
  void setSide(Side side);
        
  bool isDone();
  bool hasMoves(Side side);
  bool checkMove(Move *m, Side side);
  void doMove(Move *m, Side side);
  int countCornerPegs(Side side);
  int countPegsAdjacentToCorner(Side side);
  int countMoves(Side side);
  int countFrontierDiscs(Side side);
  int discSquares(Side side);
  int count(Side side);
  int countBlack();
  int countWhite();

  int heuristic();
  double alphabeta(bool player, double alpha, double beta, int depth);

  void setBoard(char data[]);
};

#endif
