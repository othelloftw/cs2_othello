#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"

using namespace std;

class Player {

 private:
  Side color;
  Side opponentsColor;
  Board gameBoard;
  int moveCount;

 public:
  Player(Side side);
  ~Player();
    
  Move *doMove(Move *opponentsMove, int msLeft);
  void setBoard(Board *board)
  {
    this->gameBoard = *board;
  }
  // Flag to tell if the player is running within the test_minimax context
  bool testingMinimax;
};

#endif
