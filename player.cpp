#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
  // Will be set to true in test_minimax.cpp.
  testingMinimax = false;

  this->moveCount = 0;

  this->color = side;
  this->opponentsColor = (side == BLACK) ? WHITE : BLACK;

  gameBoard.setSide(color);
}

/*
 * Destructor for the player.
 */
Player::~Player() {
  std::cerr << "Thanks for playing!";
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
  int depth = DEPTH;
  if ((msLeft < 60000) || (this->moveCount == 1)) {
    depth = 6;
  }

  gameBoard.setSide(this->color);
  gameBoard.doMove(opponentsMove, opponentsColor);
  gameBoard.alphabeta(true, MIN_SCORE, MAX_SCORE, depth);
  Move* nextMove = gameBoard.getBestMove();
  gameBoard.doMove(nextMove, color);

  this->moveCount++;

  return nextMove;
}
