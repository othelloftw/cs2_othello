#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
  taken.set(3 + 8 * 3);
  taken.set(3 + 8 * 4);
  taken.set(4 + 8 * 3);
  taken.set(4 + 8 * 4);
  black.set(4 + 8 * 3);
  black.set(3 + 8 * 4);
  
  foundMove = false; 
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
  Board *newBoard = new Board();
  newBoard->black = black;
  newBoard->taken = taken;

  newBoard->color = color;
  newBoard->opponentsColor = opponentsColor;

  newBoard->bestMove = bestMove;
  newBoard->foundMove = foundMove;
  newBoard->lastPlay = lastPlay;

  return newBoard;
}

void Board::setBestMove(Move move) {
  this->bestMove = move;
}

Move* Board::getBestMove() {
  if (foundMove){
    Move* move = new Move(this->bestMove); 
    return move;
  }
  else {
    return NULL;
  }
}

void Board::setSide(Side side) {
  this->color = side;
  if (side == BLACK) {
    this->opponentsColor = WHITE;
  }
  else {
    this->opponentsColor = BLACK;
  }
  //this->opponentsColor = (side == BLACK) ? WHITE : BLACK;
}

bool Board::occupied(int x, int y) {
  return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
  return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
  taken.set(x + 8*y);
  black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
  return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
  return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      Move move(i, j);
      if (checkMove(&move, side)) return true;
    }
  }
  return false;
}

// Heuristic score based on peg count and corner pegs.
int Board::heuristic() {
  // piece difference
  int maxPlayerPegCount = this->count(this->color);
  int minPlayerPegCount = this->count(this->opponentsColor);
  double p = 0;
  if ((maxPlayerPegCount + minPlayerPegCount) != 0) {
    if(maxPlayerPegCount > minPlayerPegCount) {
      p = 100 * maxPlayerPegCount / (maxPlayerPegCount + minPlayerPegCount);
    }
    else if(maxPlayerPegCount < minPlayerPegCount) {
      p = 100 * minPlayerPegCount / (maxPlayerPegCount + minPlayerPegCount);
    }
  }

  // corner occupancy
  double maxPlayerCorners = this->countCornerPegs(this->color);
  double minPlayerCorners = this->countCornerPegs(this->opponentsColor);
  double c = 25 * (maxPlayerCorners - minPlayerCorners);

  // corner closeness
  double l = -12.5 * (this->countPegsAdjacentToCorner(this->color) -
		      this->countPegsAdjacentToCorner(this->opponentsColor));

  // mobility
  int maxPlayerMobility = this->countMoves(this->color);
  int minPlayerMobility = this->countMoves(this->opponentsColor);
  double m = 0;
  if ((maxPlayerMobility + minPlayerMobility) != 0) {
    if(maxPlayerMobility > minPlayerMobility) {
      m = 100 * maxPlayerMobility / (maxPlayerMobility + minPlayerMobility);
    }
    else if(maxPlayerMobility < minPlayerMobility) {
      m = 100 * minPlayerMobility / (maxPlayerMobility + minPlayerMobility);
    }
  }

  // frontier discs
  double f = 0;
  int maxPlayerFrontierDiscs = this->countFrontierDiscs(this->color);
  int minPlayerFrontierDiscs = this->countFrontierDiscs(this->opponentsColor);
  if ((maxPlayerFrontierDiscs + minPlayerFrontierDiscs) != 0) {
    if (maxPlayerFrontierDiscs > minPlayerFrontierDiscs) {
      f = -100 * maxPlayerFrontierDiscs /
	(maxPlayerFrontierDiscs + minPlayerFrontierDiscs);
    }
    else if (maxPlayerFrontierDiscs < minPlayerFrontierDiscs) {
      f = 100 * minPlayerFrontierDiscs /
	(maxPlayerFrontierDiscs + minPlayerFrontierDiscs);
    }
  }

  // disc squares
  double d = discSquares(this->color);

  // weight final score
  double score = 10 * p + 801.724 * c + 382.026 * l + 78.922 * m + 74.396 * f + 10 * d;

  return score;
}

// Implement minimax with alphabeta pruning using heuristic.

double Board::alphabeta(bool player, double alpha, double beta, int depth) {
  this->foundMove = false;

  if (depth == 0) {
    return this->heuristic();
  }
  
  Side side = (player) ? this->color : this->opponentsColor;

  if (player) {
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
	Move nextMove(i, j);

	if (checkMove(&nextMove, side)) {
	  this->foundMove = true;
	  Board testBoard = *this;
	  testBoard.color = this->color;
	  testBoard.opponentsColor = this->opponentsColor;

	  testBoard.doMove(&nextMove, side);

	  double temp = alpha;
	  alpha = max(alpha, testBoard.alphabeta(false, alpha,
						 beta, depth - 1));

	  if (alpha > temp) {
	    this->setBestMove(nextMove);
	  }

	  if (beta <= alpha) {
	    goto end1;
	  }
	}	
      }
    }

  end1:
    if (!foundMove) {
      if (depth == DEPTH) {
	// no moves at root
	return 0;
      }

      if (this->lastPlay.second == side) {
	// no moves for either side, game over
	int score = this->count(this->color) - 
	  this->count(this->opponentsColor);

	
	if (score > 0) {
	  return GAME_WIN;
	}
	else if (score == 0) {
	  return 0;
	}
	else {
	  return GAME_LOSE;
	}
      }


      Board testBoard = *this;
      testBoard.color = this->color;
      testBoard.opponentsColor = this->opponentsColor;
      alpha = max(alpha, testBoard.alphabeta(false, alpha, 
					     beta, depth - 1));  
    }

    return alpha;
  }

  else {
    for (int i = 0; i < 8; i++) {
      for (int j = 0; j < 8; j++) {
	Move nextMove(i, j);

	if (checkMove(&nextMove, side)) {
	  this->foundMove = true;
	  Board testBoard = *this;
	  testBoard.color = this->color;
	  testBoard.opponentsColor = this->opponentsColor;
	  testBoard.doMove(&nextMove, side);

	  double temp = beta;
	  beta = min(beta, testBoard.alphabeta(true, alpha, 
					       beta, depth - 1));

	  if (beta < temp) {
	    this->setBestMove(nextMove);
	  }

	  if (beta <= alpha) {
	    goto end2;
	  }
	}       
      }
    }

  end2:
    if (!foundMove) {
      if (this->lastPlay.second == side) {
	// no moves for either side, game over
	int score = this->count(this->color) - 
	  this->count(this->opponentsColor);
	if (score > 0) {
	  return GAME_WIN;
	}
	else if (score == 0) {
	  return 0;
	}
	else {
	  return GAME_LOSE;
	}
      }

      Board testBoard = *this;   
      testBoard.color = this->color;
      testBoard.opponentsColor = this->opponentsColor;
      beta = min(beta, testBoard.alphabeta(true, alpha, 
					   beta, depth - 1));     
    }

    return beta;
  }    
}
          


/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
  // Passing is only legal if you have no moves.
  if (m == NULL) return !hasMoves(side);

  int X = m->getX();
  int Y = m->getY();

  // Make sure the square hasn't already been taken.
  if (occupied(X, Y)) return false;

  Side other = (side == BLACK) ? WHITE : BLACK;
  for (int dx = -1; dx <= 1; dx++) {
    for (int dy = -1; dy <= 1; dy++) {
      if (dy == 0 && dx == 0) continue;

      // Is there a capture in that direction?
      int x = X + dx;
      int y = Y + dy;
      if (onBoard(x, y) && get(other, x, y)) {
	do {
	  x += dx;
	  y += dy;
	} while (onBoard(x, y) && get(other, x, y));

	if (onBoard(x, y) && get(side, x, y)) return true;
      }
    }
  }
  return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
  // A NULL move means pass.
  if (m == NULL) return;

  // Ignore if move is invalid.
  if (!checkMove(m, side)) return;

  int X = m->getX();
  int Y = m->getY();
  Side other = (side == BLACK) ? WHITE : BLACK;
  for (int dx = -1; dx <= 1; dx++) {
    for (int dy = -1; dy <= 1; dy++) {
      if (dy == 0 && dx == 0) continue;

      int x = X;
      int y = Y;
      do {
	x += dx;
	y += dy;
      } while (onBoard(x, y) && get(other, x, y));

      if (onBoard(x, y) && get(side, x, y)) {
	x = X;
	y = Y;
	x += dx;
	y += dy;
	while (onBoard(x, y) && get(other, x, y)) {
	  set(side, x, y);
	  x += dx;
	  y += dy;
	}
      }
    }
  }
  set(side, X, Y);

  this->lastPlay = std::make_pair(m, side); 
}

// count number of pegs in corner for given side
int Board::countCornerPegs(Side side) {
  int whiteCornerPegs = 0;
  int blackCornerPegs = 0;
  int corners[4] = {0, 7, 56, 63};

  for (int i = 0; i < 4; i++) {
    if (this->black[corners[i]]) {
      blackCornerPegs++;
    }
    else if (this->taken[corners[i]]) {
      whiteCornerPegs++;
    }
  }

  if (side == BLACK) {
    return blackCornerPegs;
  }
  else {
    return whiteCornerPegs;
  }
}



// count number of pegs adjacent to corner for given side
int Board::countPegsAdjacentToCorner(Side side) {
  int whiteAdjacentCornerPegs = 0;
  int blackAdjacentCornerPegs = 0;
  int adjacentToCorner[12] = {1, 6, 8, 9, 14, 15, 48, 49, 54, 55, 57, 62};

  for (int i = 0; i < 12; i++) {
    if (this->black[adjacentToCorner[i]]) {
      blackAdjacentCornerPegs++;
    }
    else if (this->taken[adjacentToCorner[i]]) {
      whiteAdjacentCornerPegs++;
    }
  }

  if (side == BLACK) {
    return blackAdjacentCornerPegs;
  }
  else {
    return whiteAdjacentCornerPegs;
  }
}

// count number of possible moves for given side
int Board::countMoves(Side side) {
  int numMoves = 0;

  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      Move move(i, j);
      if (checkMove(&move, side)) {
	numMoves++;
      }
    }
  }

  return numMoves;
}

// count number of frontier discs for given side
int Board::countFrontierDiscs(Side side) {
  int blackFrontierDiscs = 0;
  int whiteFrontierDiscs = 0;

  for (int i = 0; i < 64; i++) {
    int adjacentSquares[8] = {i - 9, i - 8, i - 7, i - 1, i + 1, i + 7, 
			      i + 8, i + 9};
    if (this->black[i]) {      
      for (int j = 0; j < 8; j++) {
	if (adjacentSquares[j] >= 0 && adjacentSquares[j] < 64) {
	  if (!this->taken[j]) {
	    blackFrontierDiscs++;
	  }
	}
      }
    }
    else if (this->taken[i]) {
      for (int j = 0; j < 8; j++) {
	if (adjacentSquares[j] >= 0 && adjacentSquares[j] < 64) {
	  if (!this->taken[j]) {
	    whiteFrontierDiscs++;
	  }
	}
      }
    }
  }

  if (side == BLACK) {
    return blackFrontierDiscs;
  }
  else {
    return whiteFrontierDiscs;
  }
}

// return disc squares value
int Board::discSquares(Side side) {
  int d = 0;
  double dBlack = 0;
  double dWhite = 0;

  int V[64] = {20, -3, 11, 8, 8, 11, -3, 20, 
	       -3, -7, -4, 1, 1, -4, -7, -3,
	       11, -4, 2, 2, 2, 2, -4, 11, 
	       8, 1, 2, -3, -3, 2, 1, 8,
	       8, 1, 2, -3, -3, 2, 1, 8, 
	       11, -4, 2, 2, 2, 2, -4, 11,
	       -3, -7, -4, 1, 1, -4, -7, -3,
	       20, -3, 11, 8, 8, 11, -3, 20};  

  for (int i = 0; i < 64; i++) {
    if (this->black[i]) {
      dBlack += V[i];
    }
    else if (this->taken[i]) {
      dWhite += V[i];
    }
  }

  if (side == BLACK) {
    d = dBlack - dWhite;
  }
  else {
    d = dWhite - dBlack;
  }

  return d;
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
  return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
  return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
  return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
  taken.reset();
  black.reset();
  for (int i = 0; i < 64; i++) {
    if (data[i] == 'b') {
      taken.set(i);
      black.set(i);
    } if (data[i] == 'w') {
      taken.set(i);
    }
  }
}
